using System.Collections.Generic;

namespace docker_api_test
{
    public class InterfaceInfo
    {
        public string AdapterDescription { get; set; }
        public string InterfaceType { get; set; }
        public string PhysicalAddress { get; set; }
        public List<string> IPAddresses { get; set; }
    }
}