using System;
using System.Collections.Generic;
using docker_api_test;

public class HostViewModel
{
    public String ProtocolDesc { get; set; } = "http://";
    public String DomainDesc { get; set; } = "10.1.1.1";
    public String PortDesc { get; set; } = "8080";

    public String Ping { get; set; }
    public String Port { get; set; }
    public String InfoStatus { get; set; }
    public String InterfaceInfoStatus { get; set; }
    public List<Info> InfoList { get; set; }
    public List<InterfaceInfo> InterfaceInfoList { get; set; }
}