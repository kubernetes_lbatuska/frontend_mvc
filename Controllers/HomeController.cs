﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using frontend_mvc.Models;
using System.Net.Http.Json;
using System.Net.Http;
using docker_api_test;
using System.Text.Json;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace frontend_mvc.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        //private static List<HostViewModel> Hosts;
        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
            //Program.Hosts.Add(new HostViewModel());
        }

        public async Task<IActionResult> UpdateData()
        {
            foreach (var item in Program.Hosts)
            {
                item.Ping = new Ping().Send(item.DomainDesc).Status == IPStatus.Success ? "Success" : "Fail";
                try
                {
                    new TcpClient().Connect(item.DomainDesc, Int32.Parse(item.PortDesc));
                    item.Port = "Open";
                }
                catch (System.Net.Sockets.SocketException)
                {
                    item.Port = "Closed";
                }
                (List<Info>, String) InfoResult = await GetJsonHttpClient<List<Info>>(item.ProtocolDesc + item.DomainDesc + ":" + item.PortDesc + "/info", new HttpClient());
                item.InfoStatus = InfoResult.Item2;
                item.InfoList = InfoResult.Item1;
                (List<InterfaceInfo>, String) InterfaceInfoResult = await GetJsonHttpClient<List<InterfaceInfo>>(item.ProtocolDesc + item.DomainDesc + ":" + item.PortDesc + "/infointerface", new HttpClient());
                item.InterfaceInfoStatus = InterfaceInfoResult.Item2;
                item.InterfaceInfoList = InterfaceInfoResult.Item1;
            }
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Index()
        {
            foreach (var item in Program.Hosts)
            {
                if (item.Ping == "Success" && item.Port == "Open")
                {
                    continue;
                }
                item.Ping = new Ping().Send(item.DomainDesc).Status == IPStatus.Success ? "Success" : "Fail";
                try
                {
                    new TcpClient().Connect(item.DomainDesc, Int32.Parse(item.PortDesc));
                    item.Port = "Open";
                }
                catch (System.Net.Sockets.SocketException)
                {
                    item.Port = "Closed";
                }
                (List<Info>, String) InfoResult = await GetJsonHttpClient<List<Info>>(item.ProtocolDesc + item.DomainDesc + ":" + item.PortDesc + "/info", new HttpClient());
                item.InfoStatus = InfoResult.Item2;
                item.InfoList = InfoResult.Item1;
                (List<InterfaceInfo>, String) InterfaceInfoResult = await GetJsonHttpClient<List<InterfaceInfo>>(item.ProtocolDesc + item.DomainDesc + ":" + item.PortDesc + "/infointerface", new HttpClient());
                item.InterfaceInfoStatus = InterfaceInfoResult.Item2;
                item.InterfaceInfoList = InterfaceInfoResult.Item1;
            }
            return View(Program.Hosts);
        }
        [HttpGet]
        public IActionResult AddHost()
        {
            return View();
        }
        [HttpPost]
        public IActionResult AddHost(HostViewModel model)
        {
            if (ModelState.IsValid)
            {
                Program.Hosts.Add(model);
            }
            else
            {
                System.Console.WriteLine("ModelState is not valid!");
            }
            return RedirectToAction("Index");
        }
        public IActionResult HostRemove(int id)
        {
            Program.Hosts.RemoveAt(id);
            return RedirectToAction("Index");
        }
        public async Task<IActionResult> Host(int id)
        {
            //PingReply reply = new Ping().Send("10.1.1.1");
            Program.Hosts[id].Ping = new Ping().Send(Program.Hosts[id].DomainDesc).Status == IPStatus.Success ? "Success" : "Fail";
            try
            {
                new TcpClient().Connect(Program.Hosts[id].DomainDesc, Int32.Parse(Program.Hosts[id].PortDesc));
                Program.Hosts[id].Port = "Open";
            }
            catch (System.Net.Sockets.SocketException)
            {
                Program.Hosts[id].Port = "Closed";
            }
            (List<Info>, String) InfoResult = await GetJsonHttpClient<List<Info>>(Program.Hosts[id].ProtocolDesc + Program.Hosts[id].DomainDesc + ":" + Program.Hosts[id].PortDesc + "/info", new HttpClient());
            Program.Hosts[id].InfoStatus = InfoResult.Item2;
            Program.Hosts[id].InfoList = InfoResult.Item1;
            (List<InterfaceInfo>, String) InterfaceInfoResult = await GetJsonHttpClient<List<InterfaceInfo>>(Program.Hosts[id].ProtocolDesc + Program.Hosts[id].DomainDesc + ":" + Program.Hosts[id].PortDesc + "/infointerface", new HttpClient());
            Program.Hosts[id].InterfaceInfoStatus = InterfaceInfoResult.Item2;
            Program.Hosts[id].InterfaceInfoList = InterfaceInfoResult.Item1;
            return View(Program.Hosts[id]);
        }

        private static async Task<(T, String)> GetJsonHttpClient<T>(string uri, HttpClient httpClient) where T : class, new()
        {
            (T, String) Result = (new T(), "");
            try
            {
                Result.Item1 = await httpClient.GetFromJsonAsync<T>(uri);
                Result.Item2 = "Success!";
                return Result;
            }
            catch (HttpRequestException e) // Non success
            {
                Result.Item2 = "HttpRequestException Caught: " + e.Message;
            }
            catch (NotSupportedException e) // When content type is not valid
            {
                Result.Item2 = "NotSupportedException Caught: " + e.Message;
            }
            catch (JsonException e) // Invalid JSON
            {
                Result.Item2 = "JsonException Caught: " + e.Message;
            }

            return Result;
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
